CREATE DATABASE inventaris;

USE inventaris;

CREATE TABLE users (
	id int(10) NOT NULL AUTO_INCREMENT,
	username varchar(10) NOT NULL, 
	password varchar(255) NOT NULL,
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
);

CREATE TABLE profile (
	id_users int(10),
	name varchar(100),
	address text NOT NULL,
	phone_number varchar(15),
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (id_users) 
		REFERENCES users(id)
		ON UPDATE RESTRICT ON DELETE CASCADE
) ENGINE=INNODB;

CREATE TABLE items (
	id int(10) NOT NULL AUTO_INCREMENT, 
	code varchar(10) NOT NULL, 
	description text NOT NULL, 
	size VARCHAR(2) NOT NULL,
	article VARCHAR(100) NOT NULL,
	merk VARCHAR(10) NOT NULL, 
	sell_price INT(10),
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP ,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	PRIMARY KEY(id)
) ENGINE=INNODB;

CREATE TABLE activity (
	id int(10) NOT NULL AUTO_INCREMENT,
	id_users int(10) NOT NULL,
	id_items int(10) NOT NULL,
	approved_by int(10) NOT NULL,
	approved_at int(10) NOT NULL,
	note text,
	qty int(3),
	created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  updated_at DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
	FOREIGN KEY (id_users)
		REFERENCES users(id),
	FOREIGN KEY (id_items)
		REFERENCES items(id),
	PRIMARY KEY(id)
) ENGINE=INNODB;

ALTER TABLE users ADD COLUMN  last_login INT(10) AFTER password;

ALTER TABLE users ADD COLUMN  level INT(1) AFTER password;

ALTER TABLE `activity`
	CHANGE `approved_by` `approved_by` int NOT NULL DEFAULT '0',
	CHANGE `approved_at` `approved_at` int NOT NULL DEFAULT '0';