export class InputNotValidException extends Error {
  constructor(message) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
  }

  get statusCode() {
    return 400;
  }
}

export class ServerException extends Error {
  constructor(message) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
  }

  get statusCode() {
    return 500;
  }
}

export class NotFoundException extends Error {
  constructor(message) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
  }

  get statusCode() {
    return 404;
  }
}


export class DatabaseException extends Error {
  constructor(message) {
    super(message);
    Error.captureStackTrace(this, this.constructor);
    this.name = this.constructor.name;
  }

  get statusCode() {
    return 500;
  }
}
