import itemRepository from './repository.js';
import itemModel from './model.js';
import {NotFoundException} from '../../errors/errors.js';

const createItem = async (requestBody) => {
  try {
    // TODO: validate request body
    const userInput = itemModel.castUserInput(requestBody);
    const results = await itemRepository.create(userInput);
    return {
      id: results.insertId,
    };
  } catch (error) {
    throw error;
  }
};


const deleteItem = async (id) => {
  try {
    const results = await itemRepository.destroy(id);
    const {affectedRows} = results;
    if (affectedRows == 0) {
      throw new NotFoundException('Item not found');
      return;
    }
    return {
      id: parseInt(id),
    };
  } catch (error) {
    throw error;
  }
};


const getItem = async (queryParam) => {
  try {
    const results = await itemRepository.find(queryParam);
    return results;
  } catch (error) {
    throw error;
  }
};

const updateItem = async (requestBody, id) => {
  try {
    const items = await itemRepository.find({id});
    if (items.length == 0) {
      throw new NotFoundException(`Item with id ${id} not found.`);
      return;
    }
    const item = items[0];
    for (const param in item) {
      if (!Object.hasOwnProperty.call(requestBody, param)) {
        requestBody[param] = item[param];
      }
    }
    const userInput = itemModel.castUserInput(requestBody);
    // eslint-disable-next-line no-unused-vars
    const _ = await itemRepository.update(userInput);
    return {
      id: userInput.id,
    };
  } catch (error) {
    throw error;
  }
};

export default {
  createItem,
  deleteItem,
  getItem,
  updateItem,
};
