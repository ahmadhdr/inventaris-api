import itemHandler from './handler.js';
const routes = [
  {
    method: 'POST',
    path: '/items',
    handler: itemHandler.createItem,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'POST',
    path: '/items/{itemId}',
    handler: itemHandler.updateItem,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'GET',
    path: '/items',
    handler: itemHandler.getItem,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'DELETE',
    path: '/items/{itemId}',
    handler: itemHandler.deleteItem,
    options: {
      auth: 'simple',
    },
  },
];

export default routes;
