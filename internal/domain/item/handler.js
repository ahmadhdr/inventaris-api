import {NotFoundException} from '../../errors/errors.js';
import itemUsecase from './usecase.js';
import responseUtil from '../../util/response.js';

const createItem = async (request, _) => {
  const {payload} = request;
  try {
    const response = await itemUsecase.createItem(payload);
    return _.response(responseUtil.createSuccessResponse(response, 'Success create item')).code(200);
  } catch (err) {
    return _.response(responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};

const deleteItem = async (request, _) => {
  const {itemId} = request.params;
  try {
    const response = await itemUsecase.deleteItem(itemId);
    return _.response(responseUtil.createSuccessResponse(response, 'Success delete item')).code(200);
  } catch (err) {
    if (err instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(err.message, 'fail')).code(err.statusCode);
    }
    return _.response(responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};

const getItem = async (request, _) => {
  const queryParam = request.query;
  try {
    const response = await itemUsecase.getItem(queryParam);
    return _.response(responseUtil.createSuccessResponse(response, 'Success get item')).code(200);
  } catch (err) {
    return _.response( responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};


const updateItem = async (request, _) => {
  const {itemId} = request.params;
  const {payload} = request;
  try {
    const response = await itemUsecase.updateItem(payload, itemId);
    return _.response( responseUtil.createSuccessResponse(response, 'Success update item')).code(200);
  } catch (err) {
    if (err instanceof NotFoundException) {
      return _.response( responseUtil.createErrorMessage(err.message, 'fail')).code(err.statusCode);
    }
    return _.response( responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};

export default {
  createItem,
  deleteItem,
  getItem,
  updateItem,
};
