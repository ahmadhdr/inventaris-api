const castUserInput = (requestBody) => {
  const allowedField = ['code', 'description', 'size', 'article', 'merk', 'sell_price', 'id'];
  for (const field in requestBody) {
    if (Object.hasOwnProperty.call(requestBody, field)) {
      if (!allowedField.includes(field)) delete requestBody[field];
      if (['sell_price'].includes(field)) requestBody[field] = parseInt(requestBody[field]);
    }
  }
  return requestBody;
};

export default {
  castUserInput,
};
