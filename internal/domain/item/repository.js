import database from '../../common/db.js';
import util from 'util';
import mysql from 'mysql';
import dbUtil from '../../util/db.js';
const connection = database.getConnection();
const query = util.promisify(connection.query).bind(connection);

const create = async (payload) => {
  const sqlQuery = 'INSERT INTO items SET ?';
  try {
    const results = await query(sqlQuery, payload);
    return results;
  } catch (error) {
    throw error;
  }
};

const destroy = async (id) => {
  const rawSql = 'DELETE FROM ?? WHERE id = ?';
  const sqlQuery = mysql.format(rawSql, ['items', id]);
  try {
    const results = await query(sqlQuery);
    return results;
  } catch (error) {
    throw error;
  }
};

const find = async (param) => {
  const rawQuery = 'SELECT * FROM items';
  const queryParam = dbUtil.defaultQueryParam(param);
  const sqlQuery = dbUtil.buildQuery(rawQuery, queryParam);
  try {
    const results = await query(sqlQuery);
    return results;
  } catch (error) {
    throw error;
  }
};

const update = async (payload) => {
  const sqlQuery = 'UPDATE items SET description = ?, size = ?, article = ?, merk = ?, sell_price = ? WHERE id = ?';
  try {
    const results = await query(sqlQuery, [payload.description, payload.size, payload.article,
      payload.merk, payload.sell_price, payload.id]);
    return results;
  } catch (error) {
    throw error;
  }
};

export default {
  create,
  destroy,
  find,
  update,
};
