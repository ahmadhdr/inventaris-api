import database from '../../common/db.js';
import mysql from 'mysql';
import util from 'util';
import commonUtil from '../../util/common.js';
const connection = database.getConnection();
const query = util.promisify(connection.query).bind(connection);

const register = async (payload) => {
  const sql = 'INSERT INTO users SET ?';
  try {
    const result = await query(sql, payload);
    return result;
  } catch (err) {
    throw err;
  }
};

const login = async (username) => {
  const rawSql = 'SELECT * FROM ?? WHERE ?? = ?';
  const sqlQuery = mysql.format(rawSql, ['users', 'username', username]);
  try {
    const result = await query(sqlQuery);
    return result;
  } catch (err) {
    throw err;
  }
};

const updateProfile = async (userId, payload) => {
  try {
    const users = await getProfile(userId);
    if (commonUtil.isEmpty(users)) {
      const createQuery = 'INSERT INTO profile SET ?';
      const requestBody = {
        ...payload,
        id_users: userId,
      };
      // eslint-disable-next-line no-unused-vars
      const _ = await query(createQuery, requestBody);
      console.log(userId);
      return userId;
    }
    const updateQuery = 'UPDATE profile SET name = ?, address = ?, phone_number = ? WHERE id_users = ?';
    // eslint-disable-next-line no-unused-vars
    const _ = await query(updateQuery, [payload.name, payload.address, payload.phone_number, userId]);
    return userId;
  } catch (error) {
    throw error;
  }
};

const getProfile = async (userId) => {
  const rawSql = 'SELECT * FROM ?? WHERE ?? = ?';
  const sqlQuery = mysql.format(rawSql, ['profile', 'id_users', userId]);
  try {
    const result = await query(sqlQuery);
    return result;
  } catch (err) {
    throw err;
  }
};
export default {
  register,
  login,
  updateProfile,
  getProfile,
};
