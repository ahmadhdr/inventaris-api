const toUpdateProfile = (payload) => {
  const allowedField = ['name', 'address', 'phone_number'];
  for (const key in payload) {
    if (!Object.hasOwnProperty.call(payload, key)) {
      if (!allowedField.includes(key)) delete payload[key];
    }
  }
  return payload;
};

export default {
  toUpdateProfile,
};
