import {DatabaseException, NotFoundException} from '../../errors/errors.js';
import userUsecase from './usecase.js';
import responseUtil from '../../util/response.js';


const register = async (request, _) => {
  const {payload} = request;
  try {
    const response = await userUsecase.register(payload);
    return _.response(responseUtil.createSuccessResponse(response, 'Success register a user')).code(200);
  } catch (err) {
    if (err instanceof DatabaseException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    return _.response( responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};


const login = async (request, _) => {
  const {payload} = request;
  try {
    const response = await userUsecase.login(payload);
    return _.response(responseUtil.createSuccessResponse(response, 'Success login')).code(200);
  } catch (err) {
    if (err instanceof DatabaseException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    if (err instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    return _.response( responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};

const updateProfile = async (request, _) => {
  try {
    const {payload} = request;
    const {userId} = request.auth.artifacts.user;
    const response = await userUsecase.updateProfile(userId, payload);
    return _.response(responseUtil.createSuccessResponse(response, 'Success update profile')).code(200);
  } catch (err) {
    if (err instanceof DatabaseException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    if (err instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    return _.response( responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};


const getProfile = async (request, _) => {
  try {
    const {userId} = request.auth.artifacts.user;
    const response = await userUsecase.getProfile(userId);
    return _.response(responseUtil.createSuccessResponse(response, 'Success update profile')).code(200);
  } catch (err) {
    if (err instanceof DatabaseException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    if (err instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(err.message)).code(err.statusCode);
    }
    return _.response( responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};

export default {
  register,
  login,
  updateProfile,
  getProfile,
};
