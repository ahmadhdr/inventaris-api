import userHandler from './handler.js';
const routes = [
  {
    method: 'POST',
    path: '/users/register',
    handler: userHandler.register,
  },
  {
    method: 'POST',
    path: '/users/login',
    handler: userHandler.login,
  },
  {
    method: 'PUT',
    path: '/users/profile',
    handler: userHandler.updateProfile,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'GET',
    path: '/users/profile',
    handler: userHandler.getProfile,
    options: {
      auth: 'simple',
    },
  },
];

export default routes;
