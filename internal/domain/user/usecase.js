import bcrypt from 'bcrypt';
import userRepository from './repository.js';
import jwt from 'jsonwebtoken';
import {NotFoundException} from '../../errors/errors.js';
import model from './model.js';
import commonUtil from '../../util/common.js';
const SALT_ROUND = 10;
const JWT_KEY = 's3cr3t';

const register = async ({username, password, level}) => {
  const salt = bcrypt.genSaltSync(SALT_ROUND);
  const hash = bcrypt.hashSync(password, salt);
  const payload = {
    username,
    level,
    password: hash,
  };
  try {
    const results = await userRepository.register(payload);
    const accessToken = jwt.sign({userId: results.insertId}, JWT_KEY);
    return {
      accessToken,
    };
  } catch (error) {
    throw error;
  }
};

const login = async ({username, password}) => {
  try {
    const results = await userRepository.login(username);
    if (results.length == 0) throw new NotFoundException('User not found');
    const user = results[0];
    const match = bcrypt.compareSync(password, user.password);
    if (!match) {
      throw new NotFoundException('Wrong username or password');
      return;
    }
    const token = jwt.sign({userId: user.id}, JWT_KEY);
    const allowedField = ['username', 'level'];
    for (const field in user) {
      if (!allowedField.includes(field)) delete user[field];
    }
    return {
      accessToken: token,
      user,
    };
  } catch (error) {
    throw error;
  }
};

const updateProfile = async (userId, requestBody) => {
  try {
    const payload = model.toUpdateProfile(requestBody);
    const results = await userRepository.updateProfile(userId, payload);
    return {
      id: results,
    };
  } catch (error) {
    throw error;
  }
};

const getProfile = async (userId) => {
  try {
    const users = await userRepository.getProfile(userId);
    if (commonUtil.isEmpty(users)) {
      return {
        name: '',
        address: '',
        phone_number: '',
      };
    };
    const user = users[0];
    const allowedField = ['name', 'address', 'phone_number'];
    for (const key in user) {
      if (Object.hasOwnProperty.call(user, key)) {
        if (!allowedField.includes(key)) delete user[key];
      }
    }
    return user;
  } catch (error) {
    throw error;
  }
};

export default {
  register,
  login,
  updateProfile,
  getProfile,
};
