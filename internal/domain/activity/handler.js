import activityUsecase from './usecase.js';
import responseUtil from '../../util/response.js';
import {NotFoundException} from '../../errors/errors.js';

const updateActivity = async (request, _) => {
  try {
    const {payload} = request;
    const {activityId} = request.params;
    const result = await activityUsecase.updateActivity(payload, activityId);
    const response = responseUtil.createSuccessResponse(result, 'Success update activity');
    return _.response(response).code(200);
  } catch (error) {
    if (error instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(error.message, 'fail')).code(error.statusCode);
    }
    return _.response(responseUtil.createErrorMessage(error.message, 'error')).code(500);
  }
};

const createActivity = async (request, _) => {
  try {
    const {userId} = request.auth.artifacts.user;
    const {payload} = request;
    const result = await activityUsecase.createActivity(userId, payload);
    const response = responseUtil.createSuccessResponse(result, 'Success create activity');
    return _.response(response).code(200);
  } catch (error) {
    if (error instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(error.message, 'fail')).code(error.statusCode);
    }
    return _.response(responseUtil.createErrorMessage(error.message, 'error')).code(500);
  }
};

const approveActivity = async (request, _) => {
  try {
    const {activityId} = request.params;
    const {userId} = request.auth.artifacts.user;
    const result = await activityUsecase.approveActivity(activityId, userId);
    const response = responseUtil.createSuccessResponse(result, 'Success approve activity');
    return _.response(response).code(200);
  } catch (error) {
    if (error instanceof NotFoundException) {
      return _.response(responseUtil.createErrorMessage(error.message, 'fail')).code(error.statusCode);
    }
    return _.response(responseUtil.createErrorMessage(error.message, 'error')).code(500);
  }
};

const getActivity = async (request, _) => {
  const queryParam = request.query;
  try {
    const response = await activityUsecase.getActivity(queryParam);
    return _.response(responseUtil.createSuccessResponse(response, 'Success get activity')).code(200);
  } catch (err) {
    return _.response(responseUtil.createErrorMessage(err.message, 'error')).code(500);
  }
};


export default {
  createActivity,
  updateActivity,
  approveActivity,
  getActivity,
};
