import database from '../../common/db.js';
import util from 'util';
const connection = database.getConnection();
const query = util.promisify(connection.query).bind(connection);
import dbUtil from '../../util/db.js';
const create = async (payload) => {
  const sqlQuery = 'INSERT INTO activity SET ?';
  try {
    const results = await query(sqlQuery, payload);
    return results;
  } catch (error) {
    throw error;
  }
};


const findById = async (id) => {
  const rawQuery = 'SELECT * FROM activity WHERE id = ?';
  try {
    const results = await query(rawQuery, [id]);
    return results;
  } catch (error) {
    throw error;
  }
};

const find = async (param) => {
  // eslint-disable-next-line max-len
  const rawQuery = 'SELECT items.id, activity.id, items.article, items.code, activity.qty, users.username as request_by_name, activity.created_at as request_at FROM activity JOIN items ON items.id = activity.id_items JOIN users ON users.id = activity.id_users';
  const queryParam = dbUtil.defaultQueryParam(param);
  const sqlQuery = dbUtil.buildQuery(rawQuery, queryParam);
  try {
    const results = await query(sqlQuery);
    return results;
  } catch (error) {
    throw error;
  }
};

const update = async (id, payload) => {
  const sqlQuery = 'UPDATE activity SET id_items = ?, note = ?, qty = ? WHERE id = ?';
  try {
    const results = await query(sqlQuery, [payload.id_items, payload.note, payload.qty, id]);
    return results;
  } catch (error) {
    throw error;
  }
};

const approve = async (id, payload) => {
  const sqlQuery = 'UPDATE activity SET approved_by = ?, approved_at = ? WHERE id = ?';
  try {
    const results = await query(sqlQuery, [payload.approvedBy, payload.approvedAt, id]);
    return results;
  } catch (error) {
    throw error;
  }
};


export default {
  create,
  update,
  findById,
  approve,
  find,
};
