import activityRepository from './repository.js';
import itemRepository from '../item/repository.js';
import model from './model.js';
import timeUtil from '../../util/time.js';
import commonUtil from '../../util/common.js';
import {NotFoundException} from '../../errors/errors.js';

const createActivity = async (userId, requestBody) => {
  try {
    const userInput = model.castUserInput(requestBody);
    const items = await itemRepository.find({id: userInput.id_items});
    if (commonUtil.isEmpty(items)) throw new NotFoundException('Item not found');
    const payload = {
      ...userInput,
      id_users: userId,
    };
    const response = await activityRepository.create(payload);
    return {
      id: response.insertId,
    };
  } catch (error) {
    throw error;
  }
};

const updateActivity = async (requestBody, id) => {
  try {
    const items = await activityRepository.findById(id);
    if (items.length == 0) {
      throw new NotFoundException(`Activity with id ${id} not found.`);
      return;
    }
    const item = items[0];
    for (const param in item) {
      if (!Object.hasOwnProperty.call(requestBody, param)) {
        requestBody[param] = item[param];
      }
    }
    const userInput = model.castUserInput(requestBody);
    // eslint-disable-next-line no-unused-vars
    const _ = await activityRepository.update(id, userInput);
    return {
      id,
    };
  } catch (error) {
    throw error;
  }
};

const approveActivity = async (id, userId) => {
  try {
    const items = await activityRepository.findById(id);
    if (items.length == 0) {
      throw new NotFoundException(`Activity with id ${id} not found.`);
      return;
    }
    const item = items[0];
    if (item.id_users == userId) {
      throw new NotFoundException(`Cannot approve your own request activity.`);
      return;
    }
    const payload = {
      approvedBy: userId,
      approvedAt: timeUtil.getCurrentTime(),
    };
    // eslint-disable-next-line no-unused-vars
    const _ = await activityRepository.approve(id, payload);
    return {
      id,
    };
  } catch (error) {
    throw error;
  }
};

const getActivity = async (queryParam) => {
  try {
    const results = await activityRepository.find(queryParam);
    return results;
  } catch (error) {
    throw error;
  }
};

export default {
  createActivity,
  updateActivity,
  approveActivity,
  getActivity,
};
