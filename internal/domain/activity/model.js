const castUserInput = (requestBody) => {
  const allowedField = ['id_items', 'note', 'qty'];
  for (const field in requestBody) {
    if (Object.hasOwnProperty.call(requestBody, field)) {
      if (!allowedField.includes(field)) delete requestBody[field];
      if (['qty', 'id_items'].includes(field)) requestBody[field] = parseInt(requestBody[field]);
    }
  }
  return requestBody;
};


export default {
  castUserInput,
};

