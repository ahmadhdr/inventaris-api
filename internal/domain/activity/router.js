import activityHandler from './handler.js';

const routes = [
  {
    method: 'GET',
    path: '/activity',
    handler: activityHandler.getActivity,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'POST',
    path: '/activity',
    handler: activityHandler.createActivity,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'POST',
    path: '/activity/{activityId}',
    handler: activityHandler.updateActivity,
    options: {
      auth: 'simple',
    },
  },
  {
    method: 'POST',
    path: '/activity/{activityId}/approve',
    handler: activityHandler.approveActivity,
    options: {
      auth: 'simple',
    },
  },
];

export default routes;
