import hapi from '@hapi/hapi';
import hapiBearerToken from 'hapi-auth-bearer-token';
import userRouter from '../internal/domain/user/router.js';
import itemRouter from '../internal/domain/item/router.js';
import activityRouter from '../internal/domain/activity/router.js';
import middleware from './middleware/index.js';
const PORT = 5000;
const HOST = 'localhost';
const routes = [...userRouter, ...itemRouter, ...activityRouter];
const config = {
  port: PORT,
  host: HOST,
};


const start = async () => {
  const server = hapi.server({
    ...config,
    routes: {
      cors: {
        origin: ['*'], // an array of origins or 'ignore'
        headers: ['Authorization'], // an array of strings - 'Access-Control-Allow-Headers'
        exposedHeaders: ['Accept'], // an array of exposed headers - 'Access-Control-Expose-Headers',
        additionalExposedHeaders: ['Accept'], // an array of additional exposed headers
        maxAge: 60,
        credentials: true, // boolean - 'Access-Control-Allow-Credentials'
      },
    },
  });
  await server.register(hapiBearerToken);
  server.auth.strategy('simple', 'bearer-access-token', {validate: middleware.authenticated});
  server.route(routes);
  await server.start();
  console.log(`Server running at port ${PORT}`);
};

start();

