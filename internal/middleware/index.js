import jwt from 'jsonwebtoken';
const authenticated = async (request, token, h) => {
  const credentials = {token};
  const user = jwt.verify(token, 's3cr3t', (err, auth) => {
    if (err) {
      return {isValid: false, credentials};
    }
    const artifacts = {user: auth};
    return {isValid: true, credentials, artifacts};
  });
  return user;
};

export default {
  authenticated,
};
