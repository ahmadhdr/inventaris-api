const buildQuery = (query, queryParam) => {
  const queries = {};
  for (const param in queryParam) {
    if (Object.hasOwnProperty.call(queryParam, param)) {
      switch (param) {
        default:
          break;
        case 'page': case 'per_page':
          if (Object.hasOwnProperty.call(queries, 'pagination')) {
            const value = parseInt(queryParam[param]);
            const offset = (parseInt(queryParam['page']) - 1) * value;
            const currentQuery = queries['pagination'];
            queries['pagination'] = `${currentQuery} OFFSET ${offset}`;
            continue;
          }
          queries['pagination'] = `LIMIT ${parseInt(queryParam['per_page'])}`;
          break;
        case 'code':
          if (Object.hasOwnProperty.call(queries, 'where')) {
            const currentWhereClause = queries['where'];
            // TODO: escape query param
            whereClause = `${currentWhereClause} AND ${param} LIKE '%${queryParam[param]}%'`;
            continue;
          }
          queries['where'] = `WHERE ${param} LIKE '%${queryParam[param]}%'`;
          break;
        case 'id':
          if (Object.hasOwnProperty.call(queries, 'where')) {
            const currentWhereClause = queries['where'];
            // TODO: escape query param
            whereClause = `${currentWhereClause} AND ${param} = ${queryParam[param]}`;
            continue;
          }
          queries['where'] = `WHERE ${param} = ${queryParam[param]}`;
          break;
      }
    }
  }
  let newQuery = query;
  if (Object.hasOwnProperty.call(queries, 'where')) newQuery = `${newQuery} ${queries['where']}`;
  if (Object.hasOwnProperty.call(queries, 'pagination')) newQuery = `${newQuery} ${queries['pagination']}`;
  return newQuery;
};


const defaultQueryParam = (queryParam) => {
  const defaultParam = {
    page: 1,
    per_page: 10,
  };
  for (const key in defaultParam) {
    if (!Object.hasOwnProperty.call(queryParam, key)) {
      queryParam[key] = defaultParam[key];
    }
  }
  return queryParam;
};

export default {
  buildQuery,
  defaultQueryParam,
};
