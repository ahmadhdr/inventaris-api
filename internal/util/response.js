
const createErrorMessage = (message, status = 'fail') => {
  const response = {
    status: status,
    message: message,
  };
  return response;
};

const createSuccessResponse = (data, message = '') => {
  const response = {
    status: 'success',
    message,
    data,
  };
  if (message == '') delete response['message'];
  if (Object.keys(data).length == 0) delete response['data'];
  return response;
};

export default {
  createErrorMessage,
  createSuccessResponse,
};
