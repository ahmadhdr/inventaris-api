import {InputNotValidException} from '../errors/errors.js';

const isEmpty = (val) => {
  const type = typeof val;
  switch (type) {
    default:
      throw new InputNotValidException('Type not implemented');
    case 'object':
      if (Array.isArray(val)) return val.length == 0;
      break;
  }
  return false;
};

export default {
  isEmpty,
};
