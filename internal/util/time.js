const getCurrentTime = () => Math.floor(Date.now() / 1000);
export default {
  getCurrentTime,
};
