import mysql from 'mysql';
import {DatabaseException} from '../errors/errors.js';
import dbConfig from '../config/db.js';
const getConnection = () => {
  const host = dbConfig.host;
  const password = dbConfig.password;
  const user = dbConfig.user;
  const port = dbConfig.port;
  const name = dbConfig.name;

  const database = mysql.createConnection({
    host,
    password,
    user,
    port,
    database: name,
  });

  database.connect(function(error) {
    if (error) {
      console.log('Cannot connect to database');
      throw new DatabaseException('Cannot connect to database');
    }
  });
  return database;
};

export default {
  getConnection,
};
