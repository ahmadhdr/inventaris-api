# Invetaris API
This repository contains the APIs needed for project inventory

## Installation

Makesure you already installed Node JS v14 or higher.

### Setup Project
-   Clone this repository
-   Goto project folder, running **make package** to install all package needed.
- 	Run command **make config** to create configuration file needed
-   To start this project run make start command, the project will run at http://localhost:5000
-   Go to http:://localhost:5000 to makesure the project running well.

### Setup Database With Environment Local
-	Install mysql
-	Run all the queries contained in the inventory.sql file
-   Change database config file that located inside **internal/config/db.js** folder

### Setup Database With Docker
-	Run command docker compose up -d

## API documentation
Follow this instruction ***https://learning.postman.com/docs/getting-started/importing-and-exporting-data/*** this instruction to see all available API.